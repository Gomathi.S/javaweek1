package jdbcconn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class jdbcconnection {

    public static void main(String[] args) throws SQLException
    {
          //1. Make a connection with database
        Connection obj = DriverManager.getConnection("jdbc:mysql://localhost:3306/employee", "root","root");
        
        //2. Create a Statement
        Statement st=obj.createStatement();
      //  String ab="insert into empdetails values(4,'Anusha','V')";
      //  String cd="insert into empdetails values(5,'Ruchi','P')";
        String db="insert into empdetails values(6,'Anjana','S')";
        String eb="insert into empdetails values(7,'Aishu','R')";
        String fb="insert into empdetails values(8,'Yashu','V')";
        String gb="insert into empdetails values(9,'Harshitha','P')";
        String hb="insert into empdetails values(10,'Sahana','SA')";
       
    //    3. execute the queries
    //    st.execute(ab);
    //    st.execute(cd);
        st.execute(db);
        st.execute(eb);
        st.execute(fb);
        st.execute(gb);
        st.execute(hb);
    
        
        
       
        
    //4. close connection
        obj.close();
        System.out.println("record inserted successfully" );
    }
}
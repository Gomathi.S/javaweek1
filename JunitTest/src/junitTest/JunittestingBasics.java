package junitTest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JunittestingBasics {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Basics ad = new Basics();
          assertEquals(100, ad.add(50, 50));
          System.out.println("Addition");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Basics ad = new Basics();
        assertEquals(8, ad.multiply(4,2));
        System.out.println("Multiply");
	}

	@Before
	public void setUp() throws Exception {
		Basics mp=new  Basics();
        assertEquals(0,mp.div(2,4));
        System.out.println("Divide");
	}

	@After
	public void tearDown() throws Exception {
		Basics mp=new  Basics();
        assertEquals(2,mp.avg(2, 2,2));
        System.out.println("Average");
	}
	
	@Test
	public void test() {
		Basics mp=new  Basics();
        assertEquals(0,mp.sub(2, 2));
        System.out.println("subtraction");
	}
}

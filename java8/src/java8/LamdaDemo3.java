package java8;


@FunctionalInterface
interface MyInterface5
{
	//abstract method
	public double method1(String start, String end, double d, double rup);
} 
public class LamdaDemo3
{

	public static void main(String[] args)
	{
		MyInterface5 ref4 =(start,end,km,rup)->{System.out.println("Booked from "+start+" to " +end);
		return (km*rup);
		};
		System.out.println(ref4.method1("Mysore", "Bangalore", 145, 300));
	}
}

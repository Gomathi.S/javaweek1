package java8;


import java.util.*;
import java.util.stream.Stream;
public class StreamDemo
{

	public static void main(String[] args)
	{
		List<String> names = new ArrayList<>();
		names.add("Anu");
		names.add("Ruchi");
		names.add("Anjana");
		names.add("Yashu");
		names.add("Gomu");
		
		int count = 0;
		for(String str:names)
		{
			if(str.length()<5)
				count ++;
			
		}
		System.out.println(count+ " names with less than 4 char");
		
		
		//Streams and Lambda
		long count1 = names.stream().filter(str->str.length()<5).count();
		System.out.println(count1+"Names");
		
		//creating a stream using Stream.of
		Stream<String> names2 = Stream.of("Aishu","Divya");
		names2.forEach(System.out::println);
		
	}

}

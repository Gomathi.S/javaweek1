package java8;

//interface contains abstract methods... default methods and static methods
interface MyInterface{
	
	//abstract methods(without body)
	void method1();
	
	//static method
	static void method2()
	{
		System.out.println("This is static method");
	}
	
	//default method
	default void method3()
	{
		System.out.println("This is default method");
	}
}

public class Staticdefaultabstract implements MyInterface 
{
	@Override
	public void method1()
	{
		System.out.println("This is override method from MyInterface");		
		
	}
	public static void main(String[] args) 
	{
		Staticdefaultabstract obj = new Staticdefaultabstract();
		obj.method1(); // calling abstract method
		
		MyInterface.method2(); //calling static by using interface name
		
		obj.method3();	//calling default method
		
		MyInterface6 ref5 =(i,j)->{System.out.println("Addition of 2 numbers "+i+ " + " +j); // lambda 
		return (i+j);
		};
		System.out.println(ref5.method1(2,4));
		
	}
}

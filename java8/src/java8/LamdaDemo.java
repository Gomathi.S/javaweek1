package java8;

@FunctionalInterface
interface MyInterface3
{
	void method1();		//abstract method
}
public class LamdaDemo implements MyInterface3
{
	@Override
	public void method1()
	{							// override abstract method from MyInterface3
		System.out.println("Logic");
	}
	
	public static void main(String[] args) {
		
		LamdaDemo ref = new LamdaDemo();	//New method
		ref.method1();
		
		
		MyInterface3 ref1=()-> System.out.println("Some Logic"); 	// straight logic
		ref1.method1();
	}

}

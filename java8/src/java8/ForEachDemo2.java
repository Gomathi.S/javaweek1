package java8;

import java.util.*;

public class ForEachDemo2
{

	public static void main(String[] args) 
	{
		List<String> demo = new ArrayList<>();
		demo.add("one");
		demo.add("two");
		demo.add("three");
		demo.add("four");
		demo.forEach(a->System.out.println(a));

	}

}

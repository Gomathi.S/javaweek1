package java8;

interface MyInterface4
{
	void method1(String start , String end); //abstract method with some parameters
}
public class LamdaDemo2 {

	public static void main(String[] args)
	{
		MyInterface4 ref3=(start, end) -> System.out.println("I Started from "+start+" to " +end);
		ref3.method1("Mysore", "Bangalore");
	}

}

package java8;

@FunctionalInterface
interface DisplayInterface
{
	void display();		//abstract method
}
public class MethodReference3
{		
	//instance method
	public void Methods()
	{
		System.out.println("Hello");
	}
	public static void main(String[] args) 
	{
		MethodReference3 obj = new MethodReference3();	//object created
		DisplayInterface dis = obj::Methods;	//method reference using object
		dis.display();	//calling the functional interface method	
	}

}

package java8;

import java.util.Arrays;
import java.util.List;

public class StreamDemo3
{

	public static void main(String[] args)
	{
		List<Integer> nums = Arrays.asList(5,6,3,6,7,3,1);
		nums.stream()
			.sorted()
			.forEach(n->System.out.println(n));
	
	}

	}



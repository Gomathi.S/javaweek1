package java8;

import java.util.*;
import java.util.stream.Collectors;
public class StreamDemo2
{

	public static void main(String[] args)
	{
		//List of names
		List<String> names = Arrays.asList("Anusha","ruchi","anjana");
		
		//convert list to streams
		List<String> othernames = names.stream().filter(n->!"Anusha".equals(n)).collect(Collectors.toList());
		
		//Printing the resulted list
		 othernames.forEach(System.out::println);
	}

}

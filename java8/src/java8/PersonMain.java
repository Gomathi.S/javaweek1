package java8;

import java.util.*;


public class PersonMain
{

	public static void main(String[] args)
	{
		
		//Storing data in list
		List<Person> persons = Arrays.asList(
				new Person("Anusha",24),
				new Person("Ruchi",23),
				new Person("Anju",24),
				new Person("Yashu",22));
		
		//Convert to Streams //Single Condition
		Person SC = persons.stream().filter(n->"Anusha".equals(n.getName())).findAny() .orElse(null);
		System.out.println(SC);
		
		//Convert to Streams //Multiple Condition
		Person MC = persons.stream().filter((n2)->!"Anusha".equals(n2.getName()) && 23 == n2.getAge()).findAny() .orElse(null);
		System.out.println(MC);
		
		
	}

}

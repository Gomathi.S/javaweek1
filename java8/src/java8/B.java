package java8;

class A {
	public void method1()
	{
		System.out.println("Hello");
	}
}
public class B extends A
{
	@override
	public void method1()
	{
		System.out.println("World");
	}
	public static void main(String[] args) {
		B obj = new B();
		obj.method1();

	}

}

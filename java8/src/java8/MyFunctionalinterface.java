package java8;

//creating functional interface
@FunctionalInterface
interface MyInterface2
{
	void method1(); 	//Functional interface contains SINGLE ABSTRACT METHOD(SAM)

	static void method2()
	{
		System.out.println("This is static method");
	}
}

public class MyFunctionalinterface
{
	public static void main(String[] args)
	{
	MyInterface2 my=()-> System.out.println("Lambda Demo"); 	// straight logic
	my.method1();
	}
}

package java8;

interface MyInterface7
{
	void getName(String name);
}
public class MethodReference
{

	public static void main(String[] args)
	{
		//lambda expression
		MyInterface7 ref = (String s) -> System.out.println(s);
		ref.getName("Name");
		
		
	}

}

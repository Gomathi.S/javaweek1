package java8;

import java.util.Arrays;
import java.util.List;

public class ForEachDemo
{

	public static void main(String[] args)
	{
		List<Integer> n = Arrays.asList(22,44,55);
		
			n.forEach(System.out::println);

	}

}

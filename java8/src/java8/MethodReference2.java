package java8;


public class MethodReference2
{	
	
	public static void name()
	{
		System.out.println("Hello");
	}
	
	public static void main(String[] args)
	{
		Thread t = new Thread(MethodReference2::name);
		t.start();
	}

}

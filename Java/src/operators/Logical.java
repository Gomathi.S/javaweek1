package operators;

public class Logical
{

	public static void main(String[] args)
	{
		// AND &&    OR ||    NOT !
				// &&  AND - both conditions should be true 
				System.out.println((101>51)&&(21>11));
				
				//  || OR  - if anyone condition is true then its true
				System.out.println((11>6)||(11>13));

	}

}

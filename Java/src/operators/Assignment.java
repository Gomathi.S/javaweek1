package operators;

public class Assignment {

	public static void main(String[] args) {
		       // =  +=   -=  *= %=
			int n1=10;
			int n2=15;  // n2 value is 20
			
			n2=n1;   // n2 value is 10
			System.out.println(n2);
			n2+=n1; // 10+10=20    //  n2 value is 20
			System.out.println(n2);
			
			n2-=n1; // 20-10=10   // n2 value is 10
			System.out.println(n2);
			
			n2*=n1;   //10*10=100  // n2 value 100
			System.out.println(n2);
	       n2/=n1; // 100/10=10    // n2 value 10
	       System.out.println(n2);

	}

}

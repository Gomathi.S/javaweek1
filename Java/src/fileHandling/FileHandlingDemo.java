package fileHandling;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandlingDemo
{

	public static void main(String[] args) throws IOException
	{
			//create an object for File class
		File obj = new File("myFile.txt");
		obj.exists();	//check whether the file exists or not
		
		System.out.println(obj.exists());	//False
		
		obj.createNewFile();	//myfile.txt file is created
		System.out.println(obj.exists());
		
	}
}

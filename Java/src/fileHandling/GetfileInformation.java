package fileHandling;

import java.io.File;

public class GetfileInformation
{
	
	public static void main(String[] args)
	{
		File obj = new File("C:\\Users\\242166\\git\\javaweek1\\Java.txt");
		System.out.println(obj.exists());
		
		if(obj.exists())
		{
			System.out.println("Filename: "+obj.getName());
			System.out.println("MyFile Path Address: "   + obj.getAbsolutePath());
			System.out.println("Check Writeable or not: "  + obj.canExecute());
			System.out.println("Check Readable or not: "   + obj.canExecute());
			System.out.println("File size in bytes:" + obj.length());
		
		}
	}
}

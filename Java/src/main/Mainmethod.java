package main;

public class Mainmethod {
	
	public static void main()  //first main method
	{
		System.out.println("This is regular method output");
	}
	public static void main(int a)  //first main method
	{
		System.out.println("This is another main method..");
	}
 
	
	public static void main(String[] args) {  //execution starts from here
		//public -- access modifier access to all
		//static -- call without any object
		//void -- return type it doesn't return any value
		//main -- name of method
		//string[] -- array of string type arguments
		//args -- just a parameter name 
		
		System.out.println("This is main method..");
		main();
		main(10);
	}

}

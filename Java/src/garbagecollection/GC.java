package garbagecollection;

public class GC
{
//nullifying
// reference to another object...
	public void finalize()
	{
       System.out.println("unreferenced object");
	}
	public static void main(String[] args)
	{
     	 GC gd = new GC();
     	 gd=null;     //nullifying
     	 GC gd1 = new GC();
     	 gd=gd1;      //Referring to another 
     	 new GC();//Anonymous object
     	 System.gc();
   }
}

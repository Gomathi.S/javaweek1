package iterable;
import java.util.*;

import java.util.ArrayList;
import java.util.Iterator;

public class IterableExample
{

	public static void main(String[] args)
	{
		ArrayList<String> cityName = new ArrayList<String>();
		
		cityName.add("Delhi");
		cityName.add("Mumbai");
		cityName.add("Noida");
		cityName.add("Kolkata");
		
		//Iterator to iterate the cityName
		Iterator iterator  = cityName.iterator();
		System.out.println("CityName Elements:");
		
		while(iterator.hasNext())
			System.out.println(iterator.next()+ "");
		
		System.out.println();
		
	}
		

	}



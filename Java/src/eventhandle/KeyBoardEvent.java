package eventhandle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class KeyBoardEvent implements KeyListener,ActionListener
{
	static JFrame frame;
	static JTextField output;
	static JTextField input;
	public static void main(String[] args) 
	{
		
		// creating a frame
		frame= new JFrame("Keyboard Event");
		
		frame.setBackground(Color.blue);
		frame.setSize(600, 600);
		
		// creating a text field output
		output= new JTextField();
		output.setBounds(200, 300, 200,40);
		frame.add(output);

		
		// creating a text field for input
		input=new JTextField();
		input.setBounds(200,200,50,30);
		frame.add(input);
		
		// creating an exit button
		JButton exit = new JButton("Exit");
		exit.setBounds(200,200,50,30);
		frame.add(exit);
		
		// create an object for KeyBoardEvent
		KeyBoardEvent obj = new KeyBoardEvent();
		input.addKeyListener(obj);
		exit.addActionListener(obj);
		frame.setVisible(true);
		
	}
	@Override
	public void actionPerformed(ActionEvent e)
	{
		frame.dispose();
		}
	
	@Override
	public void keyTyped(KeyEvent e) 
	{
		output.setText("");
		output.setText(" Key pad : "+e.getKeyChar());
		}
	@Override
	public void keyPressed(KeyEvent e) 
	{
		output.setText("");
		output.setText("Key is pressed : "+e.getKeyCode());
		if (Character.isLetter(e.getKeyChar()))
			keyTyped(e);
		}
	@Override
	public void keyReleased(KeyEvent e) 
	{

}
}

package assignment;
import java.util.*;

public class PrimeNumber {

	private static Scanner sc;
	public static void main(String[] args)
	{
		sc = new Scanner(System.in);
		System.out.println("Enter a number-");
		int number= sc.nextInt();
		if(isPrime(number))
		{
			System.out.println(number + " is prime number");
			}
		else
		{
			System.out.println(number + " is a non-prime number");
			}
		}
	static  boolean isPrime(int num)
	{
		if(num<=1)
		{
			return false;
			}
		for(int i=2;i<=num/2;i++)
		{
			if((num%i)==0)
				return  false;
			}
		return true;
		}

}

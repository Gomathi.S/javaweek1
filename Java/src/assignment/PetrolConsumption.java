package assignment;
import java.util.*;

public class PetrolConsumption
{
		private static Scanner in;
		public static void main(String[] args)
		{
			in = new Scanner(System.in);
			System.out.print("Enter the type of vehicle bike or car : ");
			String vehicle = in.nextLine();
			System.out.print("Enter the amount of petrol: ");
			int amt = in.nextInt();
			result(amt,vehicle);
		}
		public static void result(int amount, String vehicle)
		{
			int litres = amount/115;
			int distance = 0;
			double miles;
			
			if(vehicle.equalsIgnoreCase("car"))
			{
				distance = 8*litres;
				miles =distance/1.609;
				}
			else if(vehicle.equalsIgnoreCase("bike"))
			{
				distance = 20*litres;
				 miles =distance/1.609;
				}
			else
			{
				System.out.println("Enter valid vehicle type - (bike/car)");
				return;
				}
			System.out.println("Litres of petrol the customer gets : "+litres);
			System.out.println("Total distance customer can travel in distance : "+distance);
			System.out.println("Total distance customer can travel in miles: "+ miles);
			}
		}
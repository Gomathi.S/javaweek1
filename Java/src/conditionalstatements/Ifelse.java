package conditionalstatements;

import java.util.Scanner;

public class Ifelse
{
	
			public static void main(String[] args) {
				// if-else conditional statements
				
				Scanner sc = new Scanner(System.in);
				System.out.println("Enter the age of candidate  :");
				int age = sc.nextInt();	
				
				
				// condition is true so this block of code is going execute
				if(age>18 && age<100)
				{  
					System.out.println(" Your are eligible to vote..");
				}
				
				else if (age>100)
				{
					System.out.println("you are too old so not eligible");
				}
				else 
				{
					System.out.println("You are not eligible to vote ");
				}

			}

	}

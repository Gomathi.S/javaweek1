package conditionalstatements;

import java.util.Scanner;

public class GradingElseIf
{

	public static void main(String[] args)
	{
		
			    Scanner sc= new Scanner(System.in);
				System.out.println("Enter Science Marks : ");
				int science = sc.nextInt();
				System.out.println("Enter Maths Marks  :");
				int maths = sc.nextInt();
				System.out.println("Enter Language Marks  :");
				int lang =sc.nextInt();
				
				// added all marks into single variable 'total
				int total = science+maths+lang; 
				System.out.println("Your Grand Total is  :" +total);
				
				float avg = total/3;
				System.out.println("Your average score is  : " +avg);
		          
				if (avg>=90)
				{
					System.out.println("Congrats. you got 'A' Grade ");
				}
				else if (avg>=80 && avg<90)
				{
					System.out.println("You secured 'B' Grade ");
				}
				else if(avg>=70 && avg<80)
				{
					System.out.println("You secured 'C' Grade");
				}
				else 
				{
					System.out.println("FAILED.. ");
				}
			}

		}


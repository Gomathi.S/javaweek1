package collections;
import java.util.ArrayList;
import java.util.List;
public class Arrya_Size_presentornot_add {

	public static void main(String[] args) {
               List<String> list = new ArrayList<>();
               list.add("NewYork");
               list.add("Rome");
               list.add("Paris");
               list.add("Copenhagen");
               list.add("New Delhi");
               list.add("Amsterdam");
               list.add("123456");
              
               list.add("Rome");  // to add 
               list.add(6,"Stockholm"); /// to add element at particular index number  
               list.set(5,"MUMBAI");  // to replace 
                         
               System.out.println(list);
               System.out.println("Capital city of Denmark  : "+list.get(3));
               
               list.remove(0);     // to remove 
               System.out.println(list);
               
               // to add particular position
               list.add(2, "Canberra"); // to add at particular index number  
               
               System.out.println(list);
               	// to find the size of list
               
               int tsize= list.size(); // to find the size of list
               System.out.println(tsize);  
                 
               boolean c= list.contains("12345");  // to find whether it present or not 
               System.out.println(c);
                 
               List<String> countries= new ArrayList<>();
               countries.add("India");
               countries.add("US");
               countries.add("NetherLands");
               System.out.println(countries);
               
               countries.addAll(2,list);  // to add list into another list 
               System.out.println(" After merging list with countries   : " +countries);
                 
  	}
}




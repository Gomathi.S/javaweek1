package collections;
import java.util.Collections;
import java.util.PriorityQueue;

public class PriorityQueueDemo 
{

       public static void main(String[] args) 
       {
             PriorityQueue<Integer> rollno= new PriorityQueue<>(0,Collections.reverseOrder());
             rollno.offer(205);
             rollno.offer(1);
             rollno.offer(120);
             rollno.offer(10);
             rollno.offer(40);
             rollno.offer(33);  
             rollno.offer(88);
             rollno.offer(757);
             System.out.println(rollno);
             //while(rollno.size()>0) {
             //     System.out.println(rollno.remove()+" ");
             //}
             int s= rollno.size();
             while(s>0)
             {
                    System.out.println(rollno.remove()+" ");
             }
       }

}


package collections;
import java.util.LinkedList;

public class LinkedListDemo
{
	public static void main(String[] args)
	{
		LinkedList<String>fruits = new LinkedList<>();
		
		fruits.add("Apple");
		fruits.add("Banana");
		fruits.add("Grapes");
		System.out.println(fruits.size());
		System.out.println(fruits);
		fruits.add(2,"Custard Apple");
		String s = fruits.get(1);
		System.out.println(s);
		
		LinkedList<String>animals = new LinkedList<>();
		
		fruits.add("Tiger");
		fruits.add("Lion");
		System.out.println(animals);
	}
}
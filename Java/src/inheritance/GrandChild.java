package inheritance;

public class GrandChild extends Child {

	public void gcmethod()
	{
		System.out.println("Grand Child method logic..");
	
	}
	public static void main(String[] args) {
		GrandChild g = new GrandChild();
		g.method1(); //method from parent
		g.method2(); //method from parent
		g.mymethod(); //method from child class
	}

}

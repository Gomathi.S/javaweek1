package inheritance;

public class Child extends Parent { //Inherited parent class
	
	public void mymethod() {  //its own method
		System.out.println("This is Child class method...");
	}

	public static void main(String[] args) {
		Child c = new Child();
		c.method1();  //calling parent class
		c.method2();   ///calling second method
	}

}

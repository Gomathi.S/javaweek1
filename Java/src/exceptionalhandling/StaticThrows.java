package exceptionalhandling;


public class StaticThrows 
{
	static void voter(int age)
	{
		if(age<21)
		{
			throw new ArithmeticException("you cannot vote");
			}
		else
		{
			System.out.println("You can vote");
		}
	}
		public static void main(String[] args)
		{
			voter(17);
		}
}



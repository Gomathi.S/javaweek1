package exceptionalhandling;

public class TrycatchFinally {

	public static void main(String[] args)
	{
		System.out.print(1);
		System.out.print(2);
		System.out.print(3);
		System.out.println(4);
		try { 
		System.out.println(5/0);
		}
		catch(ArithmeticException e)
		{
			System.out.println("Hello");
		}
		finally
		{
			System.out.println("Welcome to Exceptional Handling problem");
		}
		System.out.println(6);
		System.out.println(7);
		System.out.println(8);
		System.out.println(9);

	}
}
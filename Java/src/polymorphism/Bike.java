package polymorphism;

//overriding method
//creating a parent class

class vehical
{
	//defining a method
	void run()
	{
		System.out.println("Vehical is running");
	}
}
//Creating a child class
 class Bike extends vehical
 {
	 //defining the same method as in the parent class and overriding method logic
	 void run()
	 {
		 System.out.println("Bike is running safely");
	 }
 
	public static void main(String[] args) {
		Bike obj = new Bike(); //creating object
		obj.run(); //calling method
	}

}
